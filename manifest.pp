$svc_conf_template = @(END)
[Unit]
Description=Aplicacao de investimentos

[Service]
ExecStart=/usr/bin/java -jar /home/ubuntu/Api-Investimentos.jar

[Install]
WantedBy=multi-user.target

END

class { 'java' :
  package => 'openjdk-8-jdk',
}


file { '/etc/systemd/system/investimento.service':
    ensure => file,
    content => inline_epp($svc_conf_template),
}

service{'investimento':
    ensure => running,
    enable => true,
}

# gerenciar o tipo de recurso correspondente ao service acima e garantir que está rodando

class { 'nginx' : }

file { "/etc/nginx/conf.d/default.conf":
 ensure => absent,
 notify => Service['nginx'],
}

# nginx::resource::server { 'investimento':
 # listen_port => 80,
 # proxy       => 'http://localhost:8080',
 # listen_port => 8080,
 # proxy       => 'http://localhost:32000',
# }

nginx::resource::upstream { 'investimento':
  members => {
    'localhost:80' => {
      server => 'localhost',
      port   => 8080,
      weight => 1,
    },
    'localhost:8080' => {
      server => 'localhost',
      port   => 32000,
      weight => 1,
    },
  }
}

# configurar o nginx (instalação e proxy reverso)
# (tem o exemplo na documentação do módulo no forge.puppet.com)


ssh_authorized_key { 'jenkins@banana':
  ensure => present,
  user   => 'ubuntu',
  type   => 'ssh-rsa',
  key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb',
}

ssh_authorized_key { 'a2w@localhost.localdomain':
  ensure => present,
  user   => 'ubuntu',
  type   => 'ssh-rsa',
  key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABgQCocejfJ/0X8ixuQ27L3Oh9PWSb3rPOOXmjga8/RG3Xc3PY8ULMw6S8XKA6LlzkxDPTb2iUi3+zZt55Dmj5a2Q2AOS/Sn/SYEVbtUaLYY4dt1/e2H19QGVWjFmzKepeQAiGQ49Y2QwlObx9GPx0l7GNkew/gFP9Bf5kowbU4QfXzC0OgrpALlbKQk57ZDfWM2s0eiKLdD33lsvHiyJlqjgFc/orL1AiraCAea844kGCyusEYzJKMruC44YjhYCYDlnVtSRHNq2yUM/2V0rWnAnUCBjpsCUkmwgfwHwHQPa3U8h+vkqIOv7pTXCiLlWdN7vi0g7TzfrKb4nkPyLAgbKyUqUVMOTpwXYmoeeEP+qaqCgsFQrZqWODzGPPJVl6V9JuOcHU6zSVrCJOHTijMexbs7m/4f03U5Zb4AUEkL+A/W1Pzu1drDGXTwzsYH5bd8kZJfTq42zKSX7Qi/vjco8wjKchPCxLDLVAMxZmz+Hn2NjM0UZEA9EqPAnX8C1PpRk=',
}
