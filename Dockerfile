FROM openjdk:8-alpine
WORKDIR /app
COPY start.sh .
COPY target/Api-Investimentos*.jar ./investimentos.jar
CMD ["sh", "start.sh"]